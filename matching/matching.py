import sys

sys.path.append('..')
print sys.path

from data_gen.Yu_data_gen import DataGenerator
from probability.probability import Probability

class Matching:
    @staticmethod
    def check_feasible(ans):
        '''
        check whether a solution is feasible
        :param ans: a matching solution
        :return: whether the solution is good.
        '''
        # TODO: implement it.
        return True

    def __init__(self, universities = None, students = None):
        if universities is not None:
            self.universities, self.students = universities, students
        else:
            self.universities, self.students = DataGenerator().get_data()
        self.prob_esti = Probability(self.universities, self.students)

    def get_results(self):
        ans = []
        for student in self.students:
            recommendation = []
            for i in range(6):    # greedily add at most six universities to the recommendation
                maxExpectedUtility = 0
                for university in self.universities:    # find the university with maximum expected utility
                    expectedUtility = university['min_score'] * self.prob_esti.get_probability(student, university)
                    if (university not in recommendation) and (expectedUtility > maxExpectedUtility):
                        maxExpectedUtility = expectedUtility
                        bestUniversity = university
                if maxExpectedUtility > 0:
                    recommendation.append(bestUniversity)
            ans.append(recommendation)
        return ans