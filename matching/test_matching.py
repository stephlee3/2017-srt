from unittest import TestCase
import matching


class TestMatching(TestCase):
    def __init__(self, *arg1, **arg2):
        TestCase.__init__(self, *arg1, **arg2)
        # You can do something here. For example initlize a shared Matching instance.

    def test_check_feasible(self):
        self.assertTrue(matching.Matching.check_feasible([]), "wrong check")

    def test_get_results(self):
        match = matching.Matching()
        expect_ans = [[0,1] for i in match.students]
        self.assertEqual(expect_ans, match.get_results())
