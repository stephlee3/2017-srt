# Recommendation via preference

This is the project for the SRT project.
It consists of the following 4 parts: data generator, probability estimator, utility (optional) and matching process.

The author for any piece of code is in response to write the corresponding tests.
Necessary documentations are strongly encouraged.

## To execute the code

Following the example in the comment of matching/Qiao_matching.py.
This is not the final version.
Please don't really call data_gen in the current version.

## Data generator

File: data_gen/*.py

UPDATE: The new data generator has been pushed to the ZJ_data_gen branch. 

    Usage: You can obtain a list of dicts with keys ("province_id","wenli","enroll_year","diploma_id","batch","sch_id","major_id","enroll_sch_type","enroll_plan_count","people_count","min_score","max_score","avg_score","toudang_line") by calling 

        from data_gen.Yu_data_gen import DataGenerator
        data = DataGenerator().get_data()

It might be very slow when you run it for the first time. To accelerate, download .db file from https://1drv.ms/u/s!At0XIfzgWTk0qF1GkEkaI_lTcgg6 and put it into the data_gen folder before running. If you want to do fancier operations on the data, please read the function DataGenerator.rebuild() and then use sqlite3 to query the database. 

All data generators should extend the interface class DataGenerator.
The generated data (obtainable by function get_data) should follow the following format:
(https://onedrive.live.com/redir?resid=50CCAFDB33F876FE!445348&authkey=!AOy0pdf49V5Jmkw&ithint=folder%2cxlsx)
+ universities: A list of dicts. Each element of the list denotes an university, containing historical data and properties. "min_scores" and "name" are required. All features for universities should appear in university_feature.csv.
+ students: A list of dicts. Each element of the list denotes a user. The dict for each user consists of items whose keys are requirements and values are the corresponding values. "score" is required keywords. All features for students should appear in student_feature.csv.

File: data_gen/importDatabases.py

The databases can be generated in data_gen.py by DataGenerator.rebuild() slowly. Alternatively, we upload the .csv, and you can use this code to import our database from .csv.

## Probability estimator

File: probability/*.py

All probability estimators should extend the interface class ProbabilityEstimator.
The estimator can read data by data_gen.py.
The get_probability function takes a user (given by an integer), a rank of preference and a university, returns a probability.

## Utility

File: utility/*.py

The classes for implement the utility functions.

## Matching process

File: matching/*.py

All algorithms extends class Matching.
It can call data_gen, utility and probability.
get_results function returns the final assignment, which is a list of list. each sub-list is the recommended universities in order.

## Other files

+ README.md: This file
+ .gitignore: files not needed to be uploaded, usually auto-generated files.
+ init.sh: Set up the data base.
+ main.py: run the whole experiment.