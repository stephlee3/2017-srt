# -*- coding:utf-8 -*-
'''
Export:
	A folder names "prov_student" that contains students' enrollment data in 2012, 2013 and 2014 of each province (csv file).
Note:
	It exports csv files of every province in China. However, there are only data of guang_dong, hu_bei and hu_nan now.
	The folder is created in order to run cal_accept_rate.py.
'''

import sqlite3
import csv, codecs, cStringIO
import os

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """
    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()
    def writerow(self, row):
        self.writer.writerow([unicode(s).encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)
    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

provList = ['an_hui', 'bei_jing', 'chong_qing', 'fu_jian', 'gan_su', 'guang_dong', 'guang_xi', 'gui_zhou', 'hai_nan', 'he_bei', 'hei_long_jiang', 'he_nan', 'hu_bei', 'hu_nan', 'jiang_su', 'jiang_xi', 'ji_lin', 'liao_ning', 'nei_meng_gu', 'ning_xia', 'shan_dong', 'shan_xi', 'si_chuan', 'tian_jin', 'xin_jiang', 'xi_shan', 'yun_nan', 'zhe_jiang']
provChnList = ['安徽', '北京', '重庆', '福建', '甘肃', '广东', '广西', '贵州', '海南', '河北', '黑龙江', '河南', '湖北', '湖南', '江苏', '江西', '吉林', '辽宁', '内蒙古', '宁夏', '山东', '陕西', '四川', '天津', '新疆', '山西', '云南', '浙江']
path = os.getcwd()
os.makedirs(r'%s/%s'%(path,'prov_student'))        
            
conn = sqlite3.connect("ec_srt.db")
cur = conn.cursor()

for i in range(len(provList)):
    cur.execute("SELECT * FROM student WHERE year >= 2012 AND province = '%s'" % provChnList[i])
    file = open(path+'/prov_student/'+provList[i]+'.csv', "wb")
    writer = UnicodeWriter(file)
    writer.writerows(cur)
    file.close()
    
cur.close()
conn.commit()
conn.close()
