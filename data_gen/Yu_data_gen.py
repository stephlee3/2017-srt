import numpy as np
import sqlite3
import shlex #in order to recognize quotes
import random

SAMPLE_SCH = 200  # the number of users.
SAMPLE_STU = 500  # the number of users.

def newSplit(s):
    a = s[:-1].split(',')
    b = []
    s = ''
    for x in a:
        if s == '':
            if len(x) > 0 and x[0] == '"':
                s = x
            else:
                b.append(x)
        else:
            s += ',' + x
            if len(x) > 0 and x[-1] == '"':
                b.append(s)
                s = ''
    return b

class DataGenerator:
    '''
    This is the base class for data generators.
    Any data generator or probability estimator should have detailed document such that others can use it.
    '''
    def __init__(self):
        '''
        The constructor.
        '''
        import os
        conn = sqlite3.connect("./data_gen/ec_srt.db")
        cur = conn.cursor()
        tables = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name").fetchall()
        if (('school',) not in tables) or (('student',) not in tables):
            self.rebuild()
        cur.close()
        conn.commit()
        conn.close()

    def rebuild(self):
        '''
        Here we rebuild the MySQL database ec_srt contains two tables : school , student

        TABLE school: ( province varchar(20) NOT NULL, wenli int NOT NULL, year int NOT NULL, sch_name varchar(100) NOT NULL, major_name varchar(500) NOT NULL, batch int NOT NULL, plan_no int, enroll_no int, min_score int, max_score int, avg_score int)

            "sch_major_score.csv" contains info about major scores in 2010-2014 with
                (province,sch_name,wenli,batch,year,avg_score,min_score,enroll_no,major_name,max_score)

            "sch_score.csv" contains info about about overall scores of schools in 2010-2014 with
                (wenli,year,batch,enroll_no,min_score,max_score,avg_score,sch_name,province,major_name = "")

            "sch_score_2015.csv" contains info only min scores of schools in 2015 with
                (province,wenli,batch,sch_name,min_score,year,major_name = "")

            "sch_major_plan_15.csv" contains info about major plans in 2015 with
                (batch,major_name,plan_no,province,sch_name,wenli,year = 2015)

            "sch_plan_15.csv" contains info about overall plans of schools in 2015 with
                (province,wenli,batch,sch_name,plan_no, year = 2015,major_name = "")

            P.S. The info about schools will have major_name = ""

            P.S. The existence of "sch_score_2015.csv" and  "sch_plan_15.csv" leads to a WARNING:
                 there could be two pieces of info with the same (province,wenli,batch,sch_name,year,major_name)
                 QUESTION : Should we merge them in the database?

        TABLE student:( province varchar(20) NOT NULL, wenli int NOT NULL, year int NOT NULL, sch_name varchar(100), major_name varchar(500), batch int, rank int, score int, score_num int)

            "enroll_data.csv"
                (batch,province,rank,sch_name,score,score_num,major_name,wenli,year)

        P.S. Headers' meaning:

            wenli - 1 for Wen, 2 for Li

            score_num - the number of students with same score in the same major of the same school

            batch -
                1 - ben 1
                2 - ben 2
                3 - ben 2A
                4 - ben 2B
                5 - ben 3
                6 - ben 3A
                7 - ben 3B
                8 - zhuan / zhuan A
                9 - zhuan B

            main_batch -
                1 - ben 1
                2 - ben 2
                3 - ben 3
                4 - zhuan
        '''
        conn = sqlite3.connect("./data_gen/ec_srt.db")
        cur = conn.cursor()
        tables = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name").fetchall()
        if ('school',) in tables:
            cur.execute("DROP TABLE school")
        if ('student',) in tables:
            cur.execute("DROP TABLE student")
        #print "delete over"
        cur.execute(
            "CREATE TABLE school ( province VARCHAR(20) NOT NULL, wenli INT NOT NULL, year int NOT NULL, sch_name VARCHAR(100) NOT NULL, major_name VARCHAR(500) NOT NULL, batch INT NOT NULL, plan_no INT, enroll_no INT, min_score INT, max_score INT, avg_score INT)")
        cur.execute(
            "CREATE TABLE student ( batch INT, province VARCHAR(20) NOT NULL, rank INT, sch_name VARCHAR(100), score INT, score_num INT, major_name VARCHAR(500), wenli INT NOT NULL, year INT NOT NULL)")

        lines = open("./data_gen/sch_major_score.csv").readlines()
        sch_major_score = [newSplit(l) for l in lines[1:]]
        lines = open("./data_gen/sch_score.csv").readlines()
        sch_score = [newSplit(l) for l in lines[1:]]
        lines = open("./data_gen/sch_score_2015.csv").readlines()
        sch_score_2015 = [newSplit(l) for l in lines[1:]]
        lines = open("./data_gen/sch_major_plan_15.csv").readlines()
        sch_major_plan_15 = [newSplit(l) for l in lines[1:]]
        lines = open("./data_gen/sch_plan_15.csv").readlines()
        sch_plan_15 = [newSplit(l) for l in lines[1:]]


        main_batch = {
            '1' : '1',
            '2' : '2',
            '3' : '2',
            '4' : '2',
            '5' : '3',
            '6' : '3',
            '7' : '3',
            '8' : '4',
            '9' : '4',
        }

        #print "begins"
        '''i = 0
        for s in sch_major_score:
            if s.__len__() != 10:
                print s
                print i
            assert(s.__len__() == 10)
            i += 1'''
        for s in sch_major_score:
            '''if len(s[1]) > 100:
                print s[1]
            if len(s[8]) > 500:
                print s[8]'''
            s[3] = main_batch[s[3]]
            cur.execute(
                "INSERT INTO school (province,sch_name,wenli,batch,year,avg_score,min_score,enroll_no,major_name,max_score) VALUES ('%s', '%s', %s, %s, %s, %s, %s, %s, '%s', %s)"%tuple(s))
        #print "school load successfully"
        for s in sch_score:
            s[2] = main_batch[s[2]]
            cur.execute(
                "INSERT INTO school (wenli,year,batch,enroll_no,min_score,max_score,avg_score,sch_name,province,major_name) VALUES (%s, %s, %s, %s, %s, %s, %s, '%s', '%s', '')"%tuple(s))
        #print "school load successfully"
        for s in sch_score_2015:
            s[2] = main_batch[s[2]]
            cur.execute(
                "INSERT INTO school (province,wenli,batch,sch_name,min_score,year,major_name) VALUES ('%s', %s, %s, '%s', %s, %s, '')"%tuple(s))
        #print "school load successfully"
        for s in sch_major_plan_15:
            s[0] = main_batch[s[0]]
            cur.execute(
                "INSERT INTO school (batch,major_name,plan_no,province,sch_name,wenli,year) VALUES (%s, '%s', %s, '%s', '%s', %s, 2015)"%tuple(s))
        #print "school load successfully"
        for s in sch_plan_15:
            s[2] = main_batch[s[2]]
            cur.execute(
                "INSERT INTO school (province,wenli,batch,sch_name,plan_no,year,major_name) VALUES ('%s', %s, %s, '%s', %s, 2015, '')"%tuple(s))

        #print "school load successfully"

        lines = open("./data_gen/enroll_data.csv").readlines()
        enroll_data = [newSplit(l) for l in lines[1:]]

        #i = 0
        for s in enroll_data:
            '''if s.__len__() != 9:
                print s
                print i
            i += 1
            assert(s.__len__() == 9)'''
            s[8] = '20' + s[8]
            if (s[0] == '0'):
                continue
            cur.execute(
                "INSERT INTO student (batch,province,rank,sch_name,score,score_num,major_name,wenli,year) VALUES (%s, '%s', %s, '%s', %s, %s, '%s', %s, %s)"%tuple(s))


        #cur.execute("load data infile 'enroll_data.csv' into table student fields terminated by ','  optionally enclosed by '"' escaped by '"' lines terminated by '\r\n'");

        conn.commit()
        cur.close()
        conn.close()

    def get_data(self):
        conn = sqlite3.connect("./data_gen/ec_srt.db")
        cur = conn.cursor()
        cur.execute("SELECT * FROM school")
        school = []
        for s in cur:
            school.append(
                {
                    "province" : s[0],
                    "wenli" : s[1],
                    "year" : s[2],
                    "sch_name" : s[3],
                    "major_name" : s[4],
                    "batch" : s[5],
                    "plan_no" : s[6],
                    "enroll_no" : s[7],
                    "min_score" : s[8],
                    "max_score" : s[9],
                    "avg_score" : s[10],
                }
            )

        cur.execute("SELECT * FROM student")
        student = []
        for s in cur:
            student.append(
                {
                    "batch": s[0],
                    "province": s[1],
                    "rank": s[2],
                    "sch_name": s[3],
                    "score": s[4],
                    "score_num": s[5],
                    "major_name": s[6],
                    "wenli": s[7],
                    "year": s[8],
                }
            )
        return school, student

class SampleGenerator(DataGenerator):
    '''
    This is a sample generator.
    '''
    def __init__(self):
        DataGenerator.__init__(self)

        '''conn = sqlite3.connect("ec_srt_sample.db")
        cur = conn.cursor()
        tables = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name").fetchall()
        if ('school',) in tables:
            cur.execute("DROP TABLE school")
        if ('student',) in tables:
            cur.execute("DROP TABLE student")
        cur.execute(
            "CREATE TABLE school ( province VARCHAR(20) NOT NULL, wenli INT NOT NULL, year int NOT NULL, sch_name VARCHAR(100) NOT NULL, major_name VARCHAR(500) NOT NULL, batch INT NOT NULL, plan_no INT, enroll_no INT, min_score INT, max_score INT, avg_score INT)")
        cur.execute(
            "CREATE TABLE student ( batch INT, province VARCHAR(20) NOT NULL, rank INT, sch_name VARCHAR(100), score INT, score_num INT, major_name VARCHAR(500), wenli INT NOT NULL, year INT NOT NULL)")

        cur.close()
        conn.commit()
        conn.close()'''


    def get_data(self):

        '''
        conn = sqlite3.connect("ec_srt.db")
        cur = conn.cursor()

        sch_num = cur.execute("select * from school").arraysize
        sch_id = list(np.random.randint(0, sch_num - 1, SAMPLE_SCH))
        school = dict()
        for k in sch_id:
            cur.scroll(k, 'absolute')
            school += cur.fetchone()

        stu_num = cur.execute("select * from student").arraysize
        stu_id = list(np.random.randint(0, stu_num - 1, SAMPLE_STU))
        student = dict()
        for k in stu_id:
            cur.
            cur.scroll(k, 'absolute')
            student += cur.fetchone()

        conn.commit()

        return [school, student]
        '''
