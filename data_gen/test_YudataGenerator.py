from unittest import TestCase
import Yu_data_gen
import sqlite3

class TestDataGenerator(TestCase):
    def test_rebuild(self):
        generator = Yu_data_gen.DataGenerator()
        generator.rebuild()
        conn = sqlite3.connect("ec_srt.db")
        cur = conn.cursor()
        tables = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name").fetchall()
        self.assertTrue((('school',) in tables) and (('student',) in tables))


