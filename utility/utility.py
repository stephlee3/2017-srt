import sys

sys.path.append('..')

#from data_gen.data_gen import DataGenerator
import numpy as np

class Utility:
    '''
    This is the base class for utilities. Data are loaded in the constructor if needed.
    '''
    def __init__(self):
        pass
    
    def get_utility(self, person, university):
        '''
        @param person: a dict containing the basic information of a person, at least 'score'. For example {'score':700} 
        @param university: a dict containing information for a university.
        '''
        # The first thing: the utility brought by score
        item1 = 0
        if person['score'] >= university['min_score'] - 20:
            item1 = university['min_score']
        return item1
        '''
        factor1 = 60.
        item1 = 0
        if x > 15:
            item1 = factor1
        elif x < -15:
            item1 = 0
        else:
            item1 = (x + 25) ** 2 / 50.
        '''
        # cities = [person['city1'], person['city2'], person['city3'], person['city4'], person['city5']]
        # kinds = [person['kind1'], person['kind2'], person['kind3'], person['kind4'], person['kind5']]
        # # The second thing: whether in the same city; 出国率; 学校类型; 读研率; 男女比;
        # item2 = (university['city'] in cities) * 40
        # item3 = person['abroad'] * university['abroad'] * 40
        # item4 = (university['kind'] in kinds) * 40 '''language? education? medicine? sports? etc. '''
        # item5 = person['graduate'] * university['graduate'] * 40
        # item6 = university['gender'] ^ person['gender'] * 40
        # item7 = (university['type'] == person['types']) * 40 '''985? 211? normal?'''
        # item8 = (university['city'] == person['habitat']) * 10 '''tend to recommend schools in the same city, but not major factor'''