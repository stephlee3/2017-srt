import sys

sys.path.append('..')

from data_gen.Yu_data_gen import DataGenerator
from cal_probability.query_probability import Query_Probability

class Probability:
    def __init__(self, universities = None, students = None):
        # load data
        self.true_obj = Query_Probability('./cal_probability')

    def get_probability(self, person, university):
        '''
        @param person: a dict containing the basic information of a person, at least 'score'. For example {'score':700}
        @param university: a dict containing information for a university.
        '''
        return self.true_obj.query(person['province'], university['sch_name'], person['wenli'], university['batch'], person['rank'])